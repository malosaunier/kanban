package org.rygn.kanban.controller;

import org.rygn.kanban.domain.Task;
import org.rygn.kanban.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping("/tasks")
    public Collection<Task> getTasks(){
        return this.taskService.findAllTasks();
    }

    @PostMapping("/tasks")
    public ResponseEntity<Task> addTask(@Valid @RequestBody Task task){
        return new ResponseEntity<Task>(this.taskService.createTask(task), HttpStatus.CREATED);
    }

    @PatchMapping("/tasks/{move}/{id}")
    public ResponseEntity<Task> moveTask(@PathVariable("id") Long id,
                         @PathVariable("move") String move){
        if(move.equals("left")) {
            return new ResponseEntity<Task>(this.taskService.moveLeftTask(this.taskService.findTask(id)), HttpStatus.OK);
        }else if(move.equals("right")){
            return new ResponseEntity<Task>(this.taskService.moveRightTask(this.taskService.findTask(id)), HttpStatus.OK);
        }else {
            return new ResponseEntity<Task>(this.taskService.findTask(id),HttpStatus.BAD_REQUEST);
        }
    }

}
