package org.rygn.kanban.controller;

import org.rygn.kanban.domain.Developer;
import org.rygn.kanban.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeveloperController {

    @Autowired
    private DeveloperService developerService;

    @GetMapping("/developers")
    public List<Developer> getDevelopers(){
        return this.developerService.findAllDevelopers();
    }
}
