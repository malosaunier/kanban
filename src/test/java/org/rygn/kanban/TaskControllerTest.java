package org.rygn.kanban;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rygn.kanban.dao.TaskRepository;
import org.rygn.kanban.domain.Task;
import org.rygn.kanban.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
public class TaskControllerTest {

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private MockMvc mvc;

    private Task task;

    @Before
    public void initTest(){
        this.task = new Task();
        this.task.setTitle("task2");
        this.task.setNbHoursForecast(1);
        this.task.setNbHoursReal(3);
    }

    @Test
    public void getTasksTest() throws Exception{
        mvc.perform(get("/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].title", is("task1")));
    }

    @Test
    public void addTaskTest() throws Exception {
        mvc.perform( post("/tasks")
                .content(asJsonString(this.task))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        Assert.assertEquals(2, this.taskService.findAllTasks().size());
    }

    @Test
    public void updateTaskStatusTest() throws Exception {
        Long taskId = 2L;
        Long expectedStatusId = this.taskService.findTask(taskId).getStatus().getId() + 1L;
        mvc.perform(patch("/tasks/{move}/{id}", "right", taskId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        Assert.assertEquals(expectedStatusId, this.taskService.findTask(taskId).getStatus().getId());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
